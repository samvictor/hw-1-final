//
//  sequence1.cpp
//  Sequence
//
//  Created by Samuel Inniss on 9/30/13.
//  Copyright (c) 2013 Samuel Inniss. All rights reserved.
//

#include "sequence1.h"


namespace main_savitch_3
{
    //constructor
    sequence::sequence()
    {
        s_size = 0;
        s_current = -1;
    }
    
    //modification member functions
    void sequence::start()
    {
        s_current = 0; // 1st item of array
    }
    
    void sequence::advance()
    {
        s_current++;
    }
    
    void sequence::insert(const value_type& entry)
    {
        if(is_item())
        {
            for(neg_type i = s_size-1; i >= s_current; i--) // 'i' is position in old array
                    s_data[i+1] = s_data[i];
            s_data[s_current] = entry;
        }
        else
        {
            for(neg_type i = s_size-1; i >= 0; i--) // 'i' is position in old array
                    s_data[i+1] = s_data[i];
            s_data[0] = entry;
            s_current = 0;
        }
        
        s_size++;
    }
    
    void sequence::attach(const value_type& entry)
    {
        if(is_item())
        {
            for(neg_type i = s_size-1; i > s_current; i--) // 'i' is position in old array
                    s_data[i+1] = s_data[i];
            s_data[++s_current] = entry;
        }
        else
        {
            s_data[s_size] = entry;
            s_current = s_size;
        }
        
        s_size++;
    }
    
    void sequence::remove_current()
    {
        for(neg_type i = s_current; i < s_size-1; i++) // 'i' is position in new array
            s_data[i] = s_data[i+1];
        s_data[s_size-1] = 0;
        
        s_size--;
    }
    
    //constant member functions
    sequence::size_type sequence::size() const
    {
        return s_size;
    }
    
    bool sequence::is_item() const
    {
        if(s_current > -1 && s_current < s_size)
            return true;
        else
            return false;
    }
    
    sequence::value_type sequence::current() const
    {
        return s_data[s_current];
    }
    
    //SAMMY SAM
    int sequence::position() const
    {
        return s_current;
    }
}