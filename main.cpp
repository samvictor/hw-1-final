//
//  main.cpp
//  Sequence
//
//  Created by Samuel Inniss on 9/30/13.
//  Copyright (c) 2013 Samuel Inniss. All rights reserved.
//

#include <iostream>
#include <cstdlib>  // Provides size_t
#include "sequence1.h"

using std::cout;
using std::cin;
using std::endl;
using namespace main_savitch_3;

int main()
{
    char input;
    int temp_current, entry;
    
    cout << "let's start!\n";
    sequence fish;
    
    while(true)
    {
        cin >> input;
        
        switch(input){
            case 'S':
            case 's':
                fish.start();
                break;
                
            case 'A':
            case 'a':
                fish.advance();
                break;
                
            case 'I':
            case 'i':
                cout << "enter value\n";
                cin >> entry;
                fish.insert(entry);
                break;
                
            case 'T':
            case 't':
                cout << "enter value\n";
                cin >> entry;
                fish.attach(entry);
                break;
                
            case 'R':
            case 'r':
                fish.remove_current();
                break;
            
            case 'C':
            case 'c':
                cout << fish.current() << endl;
                break;
                
            case 'Z':
            case 'z':
                cout << fish.size() << endl;
                break;
        }
        
        temp_current = fish.position();
        fish.start();
        cout << endl << "Your sequence, master sam:\n";
        while(fish.is_item())
        {
            cout << fish.current() << endl;
            fish.advance();
        }
        cout << "position: " << temp_current+1 << endl;
        
        fish.start();
        while(temp_current != fish.position())
        {
            fish.advance();
        }
    }
    return 0;
}

